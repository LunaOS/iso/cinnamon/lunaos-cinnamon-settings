#!/bin/sh
dconf load / < ~/.config/lunaos-dconf.ini

rm -rf ~/.config/lunaos-dconf.ini &
rm -rf ~/.config/autostart-scripts/dconf.sh &

notify-send "Cinnamon settings applied! 🔥"
